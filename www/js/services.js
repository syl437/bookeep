'use strict';
angular.module('starter.services', [])
    .service('FileService', function ($log, $q) {

        // Public

        this.convertFileUriToFile = convertFileUriToFile;
        this.convertFileUrisToFiles = convertFileUrisToFiles;
        this.convertFileUriToInternalURL = convertFileUriToInternalURL;

        // Private

        // Converts File URI to proper Blob that will be sent to
        // server within FormData bag. We need it just because
        // iOS doesn't work good with FormData and this is the only
        // way to implement it
        function convertFileUriToFile(fileUri) {

            var deferred = $q.defer();

            // Here we use cordova file plugin that resolves fileEntry from fileUri
            window.resolveLocalFileSystemURL(fileUri, onFileResolved);

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolved(fileEntry) {

                // .file method is accepting a callback that receives file (which is instance of
                // File object that will be converted to Blob)
                fileEntry.file(function (file) {

                    // initialize FileReader
                    var reader = new FileReader();

                    // attach callback that will run upon FileReader finishes reading file
                    reader.onloadend = function (evt) {

                        deferred.resolve(new Blob([evt.target.result]), fileUri);

                    };

                    reader.onerror = function () {

                        deferred.reject();

                    };

                    // read file as array buffer
                    reader.readAsArrayBuffer(file);

                });
            }

            return deferred.promise;

        }

        function convertFileUrisToFiles(fileUris) {

            return $q.all(fileUris.map(function (file) {

                return convertFileUriToFile(file.url || file);

            }));
        }

        // Converts File URI to proper Internal URL that will be used
        // to display local device images while being in LiveReload mode
        function convertFileUriToInternalURL(fileUri) {

            var deferred = $q.defer();

            // Here we use cordova file plugin that resolves fileEntry from fileUri
            window.resolveLocalFileSystemURL(fileUri, onFileResolved, onFileResolveError);

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolved(fileEntry) {

                deferred.resolve(fileEntry.toInternalURL());
            }

            // This callback used in conjunction with window.resolveLocalFileSystemURL (see above)
            function onFileResolveError() {

                deferred.reject();
            }

            return deferred.promise;
        }

    });